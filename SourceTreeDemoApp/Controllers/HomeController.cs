﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SourceTreeDemoApp.Models;

namespace SourceTreeDemoApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            // 2
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            // adding hunk2
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}